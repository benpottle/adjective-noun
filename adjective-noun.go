package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	nounFile, err := os.Open("./nouns.txt")
	check(err)
	adjectiveFile, err := os.Open("./adjectives.txt")
	check(err)

	nounScanner := bufio.NewScanner(nounFile)
	adjectiveScanner := bufio.NewScanner(adjectiveFile)

	nounLine := nounScanner.Text()
	adjectiveLine := adjectiveScanner.Text()

	fmt.Printf(nounLine)
	fmt.Printf(adjectiveLine)
}
